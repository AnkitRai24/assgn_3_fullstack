

const express =require('express');

const mongoose= require('mongoose');

const { Comment } = require('./model/comments');

const app =express();

app.use(express.static('build'));



mongoose.connect('mongodb://localhost/assgn3_fullstack', {useNewUrlParser: true});

const connection = mongoose.connection;
connection.on('error',() => console.error( 'connection error:'));
connection.once('open', () => console.log( 'Connected to MongoDB!'));

app.use(express.json());

app.get('/comments',async (req,res) => {


    console.log('GET /comments');

    // Find all comments without any filtering (gets all comments)
    // Note: In practice you would want to paginate this response
    const comments = await Comment.find({});

    // Sent all the comments back, express will handle JSON serialization fo us
    return response.send(comments);


})

app.post('/comments', async (request, response) => {

    console.log('POST /comments ', request);

    const { email,query } = request.body;
    console.log(email)

    // If there is no text in the email, we can't go further
    if (!email) {

        return response.sendStatus(400);

    } else {

        // Surround comment creation in a try catch in case of DB or model validation issue

        try {

                Comment.findOne({email: email}).then((user) => {
                    console.log(user, 'user')
                    if (query) {
                        user.query.push(query);
                        Comment.update({email: email}, {$set: {query: user.query}}, function (err, res) {
                            if (err) throw err;
                            console.log("query getting updated")
                            return response.send("Successfully updated");
                        })
                    }

                })








        } catch  {

            await Comment.create({ query,email });
            return response.sendStatus(200);


            //(error)


            // Something unexpected went wrong
            //return response.sendStatus(500);

        }

    }

});

app.listen(5000,console.log('Server is up!!'))