const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CommentSchema = new Schema({

    email: {

        type: Schema.Types.String,
        required: true

    },
    query :{
        type: Schema.Types.Array,
        required: true

    }

}, { timestamps: true });

const Comment = mongoose.model('Comment', CommentSchema);

module.exports={Comment};